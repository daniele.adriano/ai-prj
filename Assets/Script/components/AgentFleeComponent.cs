﻿using UnityEngine;


public class AgentFleeComponent : AbstractAgentComponent
{
    public float feelSafeRange = 30;
    public float friendRange = 10;
    public int lifepointUnderPercentage = 20;
    public float updateTime = 2f;
    public float modifySpeedFactor = 0.8f;
    public float fleePointRadius = 10;

    [System.NonSerialized]
    public FSMState flee;

    Vector3 fleePoint;
    float tickTime;

    protected override void SetupState()
    {
        flee = new FSMState();
        flee.enterActions.Add(FleeEnter);
        flee.stayActions.Add(Flee);
        flee.exitActions.Add(FleeExit);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Conditions

    public bool Scared()
    {
        return
            agentController.LifePointPercentage() < agentController.targetTr.gameObject.GetComponent<AbstractAgentController>().LifePointPercentage() &&             
            agentController.LifePointPercentage() <= lifepointUnderPercentage &&
            !FriendAround();
    }

    public bool NotScared()
    {
        return
            agentController.LifePointPercentage() >= agentController.targetTr.gameObject.GetComponent<AbstractAgentController>().LifePointPercentage() ||
            agentController.LifePointPercentage() > lifepointUnderPercentage ||
            agentController.DistanceToPoint(agentController.targetTr.position) >= feelSafeRange ||
            FriendAround();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Actions

    void FleeEnter()
    {
        tickTime = Time.realtimeSinceStartup;
        agentController.StartRunning();
        GoToFleePoint();
    }

    void Flee()
    {
        Debug.DrawLine(transform.position, fleePoint, Color.yellow, 1.0f);
        if (Time.realtimeSinceStartup - tickTime >= updateTime)
        {
            if(agentController.CurrentMovementSpeed() > agentController.walkSpeed)
            {
                agentController.ModifySpeedByFactor(modifySpeedFactor);
            }            
            else
            {
                agentController.ModifyMovementSpeed(agentController.walkSpeed);
            }
            tickTime = Time.realtimeSinceStartup;            
            GoToFleePoint();
        }
    }

    void FleeExit()
    {
        agentController.StopRunning();
        agentController.RestoreOriginalSpeed();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Common methods

    void GoToFleePoint()
    {
        // Try to reach nearest friend
        GameObject nearestFriend = NearestFriend();
        if (nearestFriend != null)
        {
            fleePoint = nearestFriend.transform.position;
        }
        else
        {
            // If all friends are dead then flee away from the player 
            Vector3 vectorFromPlayer = transform.position - agentController.targetTr.position;
            fleePoint = transform.position + (vectorFromPlayer.normalized * feelSafeRange);
            fleePoint.y = transform.position.y;
            if(!agentController.NearestWalkeablePoint(fleePoint, out fleePoint, fleePointRadius))
            {
                // If walkeable point is not found then go back to agent original position
                fleePoint = agentController.OriginalPosition();
            }            
        }
        agentController.MoveTo(fleePoint);
    }

    GameObject NearestFriend()
    {
        GameObject nearest = null;
        float minDistance = float.MaxValue;
        foreach (GameObject go in gameController.enemies)
        {
            // skip the agent itself
            if (GameObject.ReferenceEquals(gameObject, go))
            {
                continue;
            }
            // Search nearest friend
            float currDistance = agentController.DistanceToPoint(go.transform.position);
            if (go.GetComponent<AbstractAgentController>().LifePointPercentage() > 0 && currDistance < minDistance)
            {
                nearest = go;
                minDistance = currDistance;
            }
        }
        return nearest;
    }

    bool FriendAround() {
        foreach (GameObject go in gameController.enemies)
        {
            // skip the agent itself
            if(GameObject.ReferenceEquals(gameObject, go))
            {
                continue;
            }
            // Search first friend in range
            if(agentController.DistanceToPoint(go.transform.position) <= friendRange)
            {
                return true;
            }
        }
        return false;
    }

}
