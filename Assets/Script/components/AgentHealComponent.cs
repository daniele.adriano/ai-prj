﻿using UnityEngine;

public class AgentHealComponent : AbstractAgentComponent
{
    
    public int healValue = 20;    
    public int startHealLifepointPercentage = 50;
    public int stopHealLifepointPercentage = 80;

    public float perceptionRange = 25;
    public float healRange = 15;

    [Header("Trigger")]
    public string healTrigger = "Heal";

    [System.NonSerialized]
    public FSMState reachWounded;
    [System.NonSerialized]
    public FSMState heal;

    //GameObject[] friends;
    GameObject woundedFriend;

    protected override void SetupState()
    {
        reachWounded = new FSMState();
        reachWounded.enterActions.Add(agentController.StartRunning);
        reachWounded.stayActions.Add(ReachWounded);
        reachWounded.exitActions.Add(agentController.StopRunning);

        heal = new FSMState();
        heal.enterActions.Add(HealEnter);
        heal.exitActions.Add(HealExit);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Conditions

    public bool WoundedFound()
    {
        woundedFriend = SearchWoundedFriend(perceptionRange);
        return woundedFriend != null;   
    }

    public bool CanHeal()
    {
        return
           NeedHealing(woundedFriend) &&
           agentController.CanSee(woundedFriend.transform.position, agentController.tag, healRange);
    }

    public bool CannotHeal()
    {        
        return
            DontNeedHealing(woundedFriend) ||
            !agentController.CanSee(woundedFriend.transform.position, agentController.tag, perceptionRange);
    }

    public bool HealDone()
    {
        return
            DontNeedHealing(woundedFriend) ||
            !agentController.CanSee(woundedFriend.transform.position, agentController.tag, healRange); 
    }

    ////////////////////////////////////////////////////////////////////////////
    // Actions

    void ReachWounded()
    {
        Debug.DrawLine(transform.position, woundedFriend.transform.position, Color.blue, 1.0f);
        agentController.MoveTo(woundedFriend.transform.position);
    }

    void HealEnter()
    {
        agentController.SetAnimatorTrigger(healTrigger);
    }

    void HealExit()
    {
        agentController.ResetAnimatorTrigger(healTrigger);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Animator events
    
    void HealEvent()
    {
        if (woundedFriend != null)
        {
            agentController.RotateToPoint(woundedFriend.transform.position);
            AbstractAgentController ac = woundedFriend.GetComponent<AbstractAgentController>();
            ac.Heal(healValue);
            Debug.DrawLine(transform.position, woundedFriend.transform.position, Color.green, 1);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Common methods

    GameObject SearchWoundedFriend(float range)
    {
        GameObject wounded = null;
        int friendLifepointPercentage = 100;
        foreach (GameObject go in gameController.enemies)
        {
            int currentFriendLifePointPercentage = go.GetComponent<AbstractAgentController>().LifePointPercentage();
            if (NeedHealing(go) &&
                // Check for wounded friend with less lifepoint percentage than the previous one
                currentFriendLifePointPercentage <= friendLifepointPercentage &&
                // Wounded friend must be in the line of sight of the healer
                 agentController.CanSee(go.transform.position, agentController.tag, range))
            {
                wounded = go;
                friendLifepointPercentage = currentFriendLifePointPercentage;
            }
        }
        return wounded;
    }

    bool NeedHealing(GameObject go)
    {
        AbstractAgentController ac = go.GetComponent<AbstractAgentController>();
        return ac.LifePointPercentage() <= startHealLifepointPercentage &&
                !ac.IsDead();
    }

    bool DontNeedHealing(GameObject go)
    {
        AbstractAgentController ac = go.GetComponent<AbstractAgentController>();
        return ac.LifePointPercentage() >= stopHealLifepointPercentage ||
            ac.IsDead();
    }

}
