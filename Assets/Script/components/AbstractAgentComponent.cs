﻿using UnityEngine;

[RequireComponent(typeof(AbstractAgentController))]
public abstract class AbstractAgentComponent : MonoBehaviour
{
    public string gameControllerTag = "GameController";

    protected GameController gameController;
    protected AbstractAgentController agentController;    

    public void Setup()
    {
        gameController = GameObject.FindGameObjectWithTag(gameControllerTag).GetComponent<GameController>();
        agentController = GetComponent<AbstractAgentController>();
        Init();
        SetupState();
    }
    
    protected abstract void SetupState();

    protected virtual void Init()
    {
        // do nothing
    }
}
