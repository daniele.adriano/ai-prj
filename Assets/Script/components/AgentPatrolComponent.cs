﻿using UnityEngine;

public class AgentPatrolComponent : AbstractAgentComponent
{    
    public Transform[] waypoints;
    public bool randomize = true;

    [System.NonSerialized]
    public FSMState patrol;

    int waypointIdx = 0;

    protected override void SetupState()
    {
        patrol = new FSMState();
        patrol.enterActions.Add(PatrolEnter);
        patrol.stayActions.Add(Patrol);
        patrol.exitActions.Add(PatrolExit);        
    }

    ////////////////////////////////////////////////////////////////////////////
    // Actions

    void PatrolEnter()
    {        
        agentController.StartWalking();
        GoToWaypoint();
    }

    void Patrol()
    {
        Debug.DrawLine(transform.position, waypoints[waypointIdx].position, Color.blue, 1.0f);
        if (agentController.DistanceToPoint(waypoints[waypointIdx].position) <= agentController.DefaultStopDistance())
        {
            GoToWaypoint();
        }
    }

    void PatrolExit()
    {
        agentController.StopWalking();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Common methods

    void GoToWaypoint()
    {
        if (randomize)
        {
            int randomIdx = Random.Range(0, waypoints.Length);
            waypointIdx = randomIdx != waypointIdx ? randomIdx : NextWaypointIdx();
        }
        else
        {
            waypointIdx = NextWaypointIdx();
        }
        agentController.MoveTo(waypoints[waypointIdx].position);
    }

    int NextWaypointIdx()
    {
        return (waypointIdx + 1) % waypoints.Length;
    }

}
