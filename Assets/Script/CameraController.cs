﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    public string targetTag = "Player";

    private Transform targetTr;
    private Vector3 offset;

    void Start()
    {
        targetTr = GameObject.FindWithTag(targetTag).transform;
        offset = transform.position - targetTr.position;             
    }


    void LateUpdate()
    {
        if (targetTr == null)
        {
            return;
        }
        Vector3 newPosition = targetTr.position + offset;
        transform.position = newPosition;
    }
}
