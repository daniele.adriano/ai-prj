﻿using UnityEngine;

public class HeathBarRotator : MonoBehaviour
{

    public float xRotation = 30;
    public float yRotation = 45;
    public float zRotation = 0;
    
    void Update()
    {
        transform.rotation = Quaternion.Euler(xRotation, yRotation, zRotation);
    }
}
