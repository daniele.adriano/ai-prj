﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    public string playerTag = "Player";
    public string enemyTag = "Enemy";

    [System.NonSerialized]
    public GameObject player;

    [System.NonSerialized]
    public GameObject[] enemies;

    void Awake()
    {
        // Even if enemies are disabled this will store original references
        enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        player = GameObject.FindGameObjectWithTag(playerTag);
    }

    public void RespawnEnemies()
    {
        foreach (GameObject enemy in enemies)
        {
            EnemyController ec = enemy.GetComponent<EnemyController>();
            ec.Respawn();
        }
    }

}
