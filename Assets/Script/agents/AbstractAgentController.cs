﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public abstract class AbstractAgentController : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////
    // Configurations
    [Header("FSM")]
    public float reactionTime = 0.1f;

    [Header("Trigger")]
    public string standTrigger = "Stand";
    public string moveTrigger = "Move";
    public string attackTrigger = "Attack";

    [Header("Statistic")]    
    public int maxLifepoint = 100;
    public int attackDamange = 20;
    public float attackDistance = 3f;
    public RectTransform healthBar;

    [Header("Head")]
    public float headYAdjustement = 1;

    [Header("Movement")]
    public float runSpeed = 6;
    public float walkSpeed = 3;
    public float walkAnimatorSpeed = 0.5f;

    ////////////////////////////////////////////////////////////////////////////

    [System.NonSerialized]
    public Transform targetTr;

    protected Vector3 originalPosition;
    protected Quaternion originalRotation;

    NavMeshAgent navMesh;
    FSM fsm;
    Animator animator;
    int lifePoint;
    float originalAnimatorSpeed;

    void OnEnable()
    {
        // Animator
        animator = GetComponent<Animator>();
        originalAnimatorSpeed = animator.speed;

        // Navmesh
        navMesh = GetComponent<NavMeshAgent>();
        navMesh.speed = runSpeed;
        navMesh.autoBraking = false;
        // tricks to simulate kinematic movement 
        navMesh.angularSpeed = 999;
        navMesh.acceleration = 999;

        // Properties
        lifePoint = maxLifepoint;
        originalPosition = transform.position;
        originalRotation = transform.rotation;

        Init();

        // FSM setup and start update loop
        fsm = new FSM(SetupFSM());
        StartCoroutine(UpdateFSM());
    }

    // Update FSM
    IEnumerator UpdateFSM()
    {
        while (true)
        {            
            fsm.Update();
            yield return new WaitForSeconds(reactionTime);
        }        
    }

    ////////////////////////////////////////////////////////////////////////////
    // Abstract methods

    protected abstract void Init();
    protected abstract FSMState SetupFSM();

    ////////////////////////////////////////////////////////////////////////////
    // Common actions

    public void StartStanding()
    {
        animator.SetTrigger(standTrigger);
    }

    public void StopStanding()
    {
        animator.ResetTrigger(standTrigger);
    }

    public void StartAttacking()
    {
        RotateToPoint(targetTr.position);
        animator.SetTrigger(attackTrigger);
    }

    public void StopAttacking()
    {
        animator.ResetTrigger(attackTrigger);
    }

    public void StartRunning()
    {
        animator.SetTrigger(moveTrigger);
        animator.speed = originalAnimatorSpeed;
        navMesh.speed = runSpeed;
    }

    public void StopRunning()
    {
        animator.ResetTrigger(moveTrigger);
        StopMoving();        
    }

    public void StartWalking()
    {
        // TODO Workaround because there is not a walk animation. To create walk animation duplicate and modify run animation 
        animator.SetTrigger(moveTrigger);
        animator.speed = walkAnimatorSpeed;
        navMesh.speed = walkSpeed;
    }

    public void StopWalking()
    {
        animator.ResetTrigger(moveTrigger);        
        StopMoving();
        // Restore default speed values
        RestoreOriginalSpeed();
    }

    public void RestoreOriginalOrientation()
    {
        transform.rotation = originalRotation;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Lifepoint stuff

    public void Heal(int healPoint)
    {
        lifePoint += healPoint;
        lifePoint = lifePoint > maxLifepoint ? maxLifepoint : lifePoint;
        UpdateHealthBar();
    }

    public void Rest()
    {
        lifePoint = maxLifepoint;
        UpdateHealthBar();
    }

    public bool IsDead()
    {
        return lifePoint <= 0;
    }

    public void Damage(int hitPoint)
    {
        lifePoint -= hitPoint;
        UpdateHealthBar();
    }

    public void UpdateHealthBar()
    {
        if(healthBar != null)
        {
            healthBar.sizeDelta = new Vector2(LifePointPercentage(), healthBar.sizeDelta.y);
        }
    }

    public int LifePointPercentage()
    {
        return lifePoint * 100 / maxLifepoint;
    }

    public void Respawn()
    {
        gameObject.SetActive(false);
        transform.position = originalPosition;
        RestoreOriginalOrientation();
        RestoreOriginalSpeed();
        Rest();
        gameObject.SetActive(true);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Utilities

    public void SetAnimatorTrigger(string trigger)
    {
        animator.SetTrigger(trigger);
    }

    public void ResetAnimatorTrigger(string trigger)
    {
        animator.ResetTrigger(trigger);
    }

    public void RestoreOriginalSpeed()
    {
        animator.speed = originalAnimatorSpeed;
        navMesh.speed = runSpeed;
    }

    public void MoveTo(Vector3 point)
    {
        navMesh.destination = point;
    }

    public void StopMoving()
    {
        navMesh.isStopped = true;
        navMesh.ResetPath();
        navMesh.isStopped = false;
    }

    public float DistanceToPoint(Vector3 point)
    {
        return Vector3.Distance(transform.position, point);
    }

    public void RotateToPoint(Vector3 point)
    {
        transform.LookAt(point);
    }

    public float DefaultStopDistance()
    {
        return navMesh.stoppingDistance;
    }    

    public Vector3 OriginalPosition()
    {
        return originalPosition;
    }

    public bool NearestWalkeablePoint(Vector3 position, out Vector3 walkeablePosition, float walkRadius)
    {
        NavMeshHit hit;
        if(NavMesh.SamplePosition(position, out hit, walkRadius, NavMesh.AllAreas))
        {
            walkeablePosition = hit.position;
            return true;
        }
        walkeablePosition = Vector3.zero;
        return false;
    }

    public float CurrentMovementSpeed()
    {
        return navMesh.speed;
    }

    public void ModifyMovementSpeed(float speed)
    {
        navMesh.speed = speed;
    }

    public void ModifySpeedByFactor(float factor)
    {
        navMesh.speed = navMesh.speed * factor;
        animator.speed = animator.speed * factor;        
    }

    public bool CanSee(Vector3 targetPoint, string expectedTag, float range)
    {
        if(transform.position.Equals(targetPoint))
        {
            return true;
        }
        if (DistanceToPoint(targetPoint) > range)
        {
            return false;
        }
        // TODO use target head y adjustement
        Vector3 targetHead = new Vector3(targetPoint.x, targetPoint.y + headYAdjustement, targetPoint.z);
        Vector3 head = new Vector3(transform.position.x, transform.position.y + headYAdjustement, transform.position.z);               
        RaycastHit hit;
        if (Physics.Raycast(head, targetHead - head, out hit, range))
        {
            return expectedTag.CompareTo(hit.transform.tag) == 0;
        }
        return false;
    }

}
