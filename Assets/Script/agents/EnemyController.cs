﻿using UnityEngine;

public class EnemyController : AbstractAgentController
{
    ////////////////////////////////////////////////////////////////////////////
    // Configurations

    public float aggroRange = 10f;
    public float loseAggroRange = 25f;    

    [Header("Tags")]
    public string playerTag = "Player";

    ////////////////////////////////////////////////////////////////////////////    

    AgentPatrolComponent patrolComponent;
    AgentFleeComponent fleeComponent;
    AgentHealComponent healComponent;

    protected override void Init()
    {        
        targetTr = GameObject.FindWithTag(playerTag).transform;
        patrolComponent = GetComponent<AgentPatrolComponent>();        
        fleeComponent = GetComponent<AgentFleeComponent>();
        healComponent = GetComponent<AgentHealComponent>();
    }

    protected override FSMState SetupFSM()

    {
        // Standard enemy states and transitions
        FSMState stand = new FSMState();
        stand.enterActions.Add(StartStanding);
        stand.exitActions.Add(StopStanding);       
        FSMState reachTarget = new FSMState();
        reachTarget.enterActions.Add(StartRunning);
        reachTarget.stayActions.Add(ReachTarget);
        reachTarget.exitActions.Add(StopRunning);
        FSMState attack = new FSMState();
        attack.enterActions.Add(StartAttacking);
        attack.exitActions.Add(StopAttacking);
        FSMState backToBase = new FSMState();
        backToBase.enterActions.Add(BackToBaseEnter);
        backToBase.stayActions.Add(BackToBase);
        backToBase.exitActions.Add(BackToBaseExit);
        FSMState dead = new FSMState();
        dead.enterActions.Add(DeadEnter);

        FSMTransition canAggro = new FSMTransition(CanAggro);        
        FSMTransition canAttack = new FSMTransition(CanAttack);
        FSMTransition cannotAttack = new FSMTransition(CannotAttack);
        FSMTransition aggroLosed = new FSMTransition(AggroLosed);
        FSMTransition baseReached = new FSMTransition(BaseReached, new FSMAction[]{ RestoreOriginalOrientation, Rest });
        FSMTransition isDead = new FSMTransition(IsDead);

        // Default state
        FSMState initialState = stand;

        // Patroller behaviour
        if (patrolComponent != null && patrolComponent.enabled)
        {
            patrolComponent.Setup();
            initialState = patrolComponent.patrol;
            patrolComponent.patrol.AddTransition(isDead, dead);
            patrolComponent.patrol.AddTransition(canAttack, attack);
            patrolComponent.patrol.AddTransition(canAggro, reachTarget);
        }

        // Coward enemy behaviour
        if (fleeComponent != null && fleeComponent.enabled)
        {
            fleeComponent.Setup();
            FSMTransition scared = new FSMTransition(fleeComponent.Scared);
            FSMTransition notScared = new FSMTransition(fleeComponent.NotScared);

            // Check if is scared
            stand.AddTransition(scared, fleeComponent.flee);
            reachTarget.AddTransition(scared, fleeComponent.flee);
            attack.AddTransition(scared, fleeComponent.flee);
            backToBase.AddTransition(scared, fleeComponent.flee);

            // Flee away
            fleeComponent.flee.AddTransition(isDead, dead);
            fleeComponent.flee.AddTransition(notScared, backToBase);
        }

        // Healer behaviour
        if (healComponent != null && healComponent.enabled)
        {
            healComponent.Setup();
            FSMTransition woundedFound = new FSMTransition(healComponent.WoundedFound);
            FSMTransition canHeal = new FSMTransition(healComponent.CanHeal);
            FSMTransition cannotHeal = new FSMTransition(healComponent.CannotHeal);
            FSMTransition healingDone = new FSMTransition(healComponent.HealDone);

            // Search for wounded friend
            stand.AddTransition(woundedFound, healComponent.reachWounded);
            reachTarget.AddTransition(woundedFound, healComponent.reachWounded);
            attack.AddTransition(woundedFound, healComponent.reachWounded);            
            backToBase.AddTransition(woundedFound, healComponent.reachWounded);
            if (patrolComponent != null && patrolComponent.enabled)
            {
                patrolComponent.patrol.AddTransition(woundedFound, healComponent.reachWounded);
            }

            // Reach wounded friend
            healComponent.reachWounded.AddTransition(isDead, dead);
            healComponent.reachWounded.AddTransition(canHeal, healComponent.heal);
            healComponent.reachWounded.AddTransition(cannotHeal, backToBase);

            // Heal friend
            healComponent.heal.AddTransition(isDead, dead);
            healComponent.heal.AddTransition(healingDone, backToBase);
        }

        // Standard enemy behaviour
        stand.AddTransition(isDead, dead);
        stand.AddTransition(canAttack, attack);
        stand.AddTransition(canAggro, reachTarget);

        attack.AddTransition(isDead, dead);
        attack.AddTransition(cannotAttack, reachTarget);

        reachTarget.AddTransition(isDead, dead);
        reachTarget.AddTransition(canAttack, attack);
        reachTarget.AddTransition(aggroLosed, backToBase);

        backToBase.AddTransition(isDead, dead);
        backToBase.AddTransition(canAttack, attack);
        backToBase.AddTransition(canAggro, reachTarget);
        backToBase.AddTransition(baseReached, initialState);
        
        return initialState;
    }


    ////////////////////////////////////////////////////////////////////////////
    // Conditions

    bool CanAttack()
    {
        return
            // Attack the enemy even if a friend is in line of sight
            CanSee(targetTr.position, playerTag, attackDistance) ||
            CanSee(targetTr.position, transform.tag, attackDistance);
    }

    bool CannotAttack()
    {
        return !CanAttack();
    }

    bool CanAggro()
    {
        return CanSee(targetTr.position, playerTag, aggroRange);
    }

    bool AggroLosed()
    {
        return DistanceToPoint(targetTr.position) >= loseAggroRange;
    }

    bool BaseReached()
    {
        return DistanceToPoint(originalPosition) <= DefaultStopDistance();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Actions

    void ReachTarget()
    {
        Debug.DrawLine(transform.position, targetTr.position, Color.black, 0.5f);
        MoveTo(targetTr.position);
    }

    void BackToBaseEnter()
    {
        StartWalking();
        MoveTo(originalPosition);
    }

    void BackToBase()
    {
        Debug.DrawLine(transform.position, originalPosition, Color.blue, 1);
    }

    void BackToBaseExit()
    {
        StopWalking();
    }

    void DeadEnter()
    {
        gameObject.SetActive(false);
    }     

    ////////////////////////////////////////////////////////////////////////////
    // Animator events

    void AttackEvent()
    {
        RotateToPoint(targetTr.position);
        targetTr.GetComponent<AbstractAgentController>().Damage(attackDamange);
        Debug.DrawLine(transform.position, targetTr.position, Color.red, 0.5f);
    }    

}
