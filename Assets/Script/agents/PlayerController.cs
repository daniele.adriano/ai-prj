﻿using UnityEngine;

public class PlayerController : AbstractAgentController
{
    ////////////////////////////////////////////////////////////////////////////
    // Configurations
    public float itemStopDistance = 3;

    [Header("Tags")]
    public string terrainTag = "Terrain";
    public string enemyTag = "Enemy";
    public string itemTag = "Item";

    ////////////////////////////////////////////////////////////////////////////    

    GameController gameController;
    Vector3 terrainPosition;
    bool clickedTerrain;

    protected override void Init() 
    {
        GameObject controller = GameObject.FindGameObjectWithTag("GameController");
        gameController = controller.GetComponent<GameController>();
    }

    protected override FSMState SetupFSM()
    {      
        // FSM State
        FSMState stand = new FSMState();
        stand.enterActions.Add(StartStanding);
        stand.exitActions.Add(StopStanding);
        FSMState reachTarget = new FSMState();
        reachTarget.enterActions.Add(StartRunning);
        reachTarget.stayActions.Add(ReachTarget);
        reachTarget.exitActions.Add(StopRunning);
        FSMState attack = new FSMState();
        attack.enterActions.Add(StartAttacking);
        attack.exitActions.Add(StopAttacking);
        FSMState dead = new FSMState();
        dead.enterActions.Add(DeadEnter);

        // FSM Transitions
        FSMTransition canReachTarget = new FSMTransition(CanReachTarget);
        FSMTransition targetReached = new FSMTransition(TargetReached);
        FSMTransition canAttack = new FSMTransition(CanAttack);
        FSMTransition canUseItem = new FSMTransition(ItemReached, new FSMAction[] { UseItem });
        FSMTransition cannotAttack = new FSMTransition(CannotAttack);
        FSMTransition isDead = new FSMTransition(IsDead);

        // FSM Links between states and transictions
        stand.AddTransition(isDead, dead);
        stand.AddTransition(canAttack, attack);
        stand.AddTransition(canUseItem, stand);
        stand.AddTransition(canReachTarget, reachTarget);
        reachTarget.AddTransition(isDead, dead);
        reachTarget.AddTransition(canAttack, attack);
        reachTarget.AddTransition(targetReached, stand);
        attack.AddTransition(isDead, dead);
        attack.AddTransition(cannotAttack, stand);

        return stand;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                targetTr = hit.transform;

                //Debug.Log("Clicked: " + targetTr.tag + " Tranform position " + targetTr.position);
                clickedTerrain = terrainTag.CompareTo(targetTr.tag) == 0;
                if (clickedTerrain && !NearestWalkeablePoint(hit.point, out terrainPosition, 1))
                {
                    targetTr = null;
                }                
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Conditions

    protected bool TargetReached()
    {
        return
            targetTr == null ||
            CanAttack() ||
            ItemReached() ||
            TerrainReached();
    }

    protected bool CanReachTarget()
    {
        return
            targetTr != null &&
            // The player agent itself should not be reached
            !transform.tag.Equals(targetTr.tag) && 
            !TargetReached();
    }

    protected bool CanAttack()
    {
        return
            targetTr != null &&
            enemyTag.CompareTo(targetTr.tag) == 0 &&
            DistanceToPoint(targetTr.position) <= attackDistance;
    }

    protected bool CannotAttack()
    {
        return !CanAttack();
    }

    bool ItemReached()
    {
        return
            targetTr != null &&
            itemTag.CompareTo(targetTr.tag) == 0 &&
            DistanceToPoint(targetTr.position) <= itemStopDistance;
    }

    bool TerrainReached()
    {
        return clickedTerrain && DistanceToPoint(terrainPosition) <= DefaultStopDistance();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Actions

    void ReachTarget()
    {
        MoveTo(clickedTerrain ? terrainPosition : targetTr.position);
    }   

    void UseItem()
    {
        AbstractItem item = targetTr.GetComponent<AbstractItem>();
        if(item != null)
        {
            item.Use(this);
        }        
        // Item used, to use again a new click on the item is required
        targetTr = null;
    }

    void DeadEnter()
    {
        // Respawn player
        targetTr = null;
        Respawn();

        // Respawn enemies
        gameController.RespawnEnemies();
    }


    ////////////////////////////////////////////////////////////////////////////
    // Animator events

    void AttackEvent()
    {        
        AbstractAgentController enemy = targetTr.GetComponent<AbstractAgentController>();
        if (enemy != null)
        {
            enemy.Damage(attackDamange);
        }

        // Attack action done, to attack again click on the enemy
        targetTr = null;
    }

}

