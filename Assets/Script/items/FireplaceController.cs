﻿using UnityEngine;

public class FireplaceController : AbstractItem 
{
    GameController gameController;

    private void Start()
    {
        GameObject controller = GameObject.FindGameObjectWithTag("GameController");
        gameController = controller.GetComponent<GameController>();
    }

    public override void Use(PlayerController player)
    {
        player.Rest();
        gameController.RespawnEnemies();
    }
}
