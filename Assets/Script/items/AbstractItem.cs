﻿using UnityEngine;

public abstract class AbstractItem : MonoBehaviour
{

    public abstract void Use(PlayerController player);
    
}
